package com.ukefu.webim.config.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.UKTools;
import com.ukefu.util.ai.DicSegment;
import com.ukefu.util.sensitive.SensitiveFilter;
import com.ukefu.webim.service.cache.CacheHelper;
import com.ukefu.webim.service.repository.BlackListRepository;
import com.ukefu.webim.service.repository.GenerationRepository;
import com.ukefu.webim.service.repository.SysDicRepository;
import com.ukefu.webim.service.repository.SystemConfigRepository;
import com.ukefu.webim.service.repository.TablePropertiesRepository;
import com.ukefu.webim.service.repository.UserRepository;
import com.ukefu.webim.web.model.BlackEntity;
import com.ukefu.webim.web.model.Generation;
import com.ukefu.webim.web.model.SysDic;
import com.ukefu.webim.web.model.SystemConfig;
import com.ukefu.webim.web.model.User;

@Component
@ImportResource("classpath:config/applicationContext-snaker.xml")
public class StartedEventListener implements ApplicationListener<ContextRefreshedEvent> {
	
	@Value("${web.upload-path}")
    private String path;
	
	@Value("${spring.datasource.driver-class-name}")
    private String drivers_class_name;
	
	private SysDicRepository sysDicRes;
	
	private BlackListRepository blackListRes ;

	@Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
    	ConvertUtils.register(new DateConverter(null), java.util.Date.class);
    	if(UKDataContext.getContext() == null){
    		UKDataContext.setApplicationContext(event.getApplicationContext());
    	}
    	sysDicRes = event.getApplicationContext().getBean(SysDicRepository.class) ;
    	blackListRes = event.getApplicationContext().getBean(BlackListRepository.class) ;
    	List<SysDic> sysDicList = sysDicRes.findAll() ;
    	for(SysDic dic : sysDicList){
    		CacheHelper.getSystemCacheBean().put(dic.getId(), dic, dic.getOrgi());
			if(dic.getParentid().equals("0")){
				List<SysDic> sysDicItemList = new ArrayList<SysDic>();
				for(SysDic item : sysDicList){
					if(item.getDicid()!=null && item.getDicid().equals(dic.getId())){
						sysDicItemList.add(item) ;
					}
				}
				Collections.sort(sysDicItemList , new Comparator<SysDic>() {
					@Override
					public int compare(SysDic o1, SysDic o2) {
						return o1!=null && o2!=null ? o1.getSortindex() - o2.getSortindex() : o1!=null ? 1 : o2!=null ? 1 : 0;
					}
				});
				CacheHelper.getSystemCacheBean().put(dic.getCode(), sysDicItemList, dic.getOrgi());
			}
		}
    	List<BlackEntity> blackList = blackListRes.findByOrgi(UKDataContext.SYSTEM_ORGI) ;
    	for(BlackEntity black : blackList){
    		if(!StringUtils.isBlank(black.getUserid())) {
	    		if(black.getEndtime()==null || black.getEndtime().after(new Date())){
	    			CacheHelper.getSystemCacheBean().put(black.getUserid(), black, black.getOrgi());
	    		}
    		}
    	}
    	/**
    	 * 加载系统全局配置
    	 */
    	SystemConfigRepository systemConfigRes = event.getApplicationContext().getBean(SystemConfigRepository.class) ;
    	List<SystemConfig> configList = systemConfigRes.findByOrgi(UKDataContext.SYSTEM_ORGI) ;
    	if(configList != null && configList.size() > 0){
    		CacheHelper.getSystemCacheBean().put("systemConfig", configList.get(0), UKDataContext.SYSTEM_ORGI);
    	}
    	GenerationRepository generationRes = event.getApplicationContext().getBean(GenerationRepository.class) ;
    	List<Generation> generationList = generationRes.findAll() ;
    	for(Generation generation : generationList){
    		CacheHelper.getSystemCacheBean().setAtomicLong(UKDataContext.ModelType.WORKORDERS.toString(), generation.getStartinx());
    	}
    	
    	UKTools.initSystemArea();
    	
    	UKTools.initSystemSecField(event.getApplicationContext().getBean(TablePropertiesRepository.class));
    	//UKTools.initAdv();//初始化广告位
    	
    	/**
    	 * 初始化敏感词词库
    	 */
    	SensitiveFilter.getSensitiveFilter().initSensitiveWords();
    	
    	try {
			DicSegment.segment("") ;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//初始化
    	
    	//把当前数据库类型放入缓存
		if(!StringUtils.isBlank(drivers_class_name) && CacheHelper.getSystemCacheBean().getCacheObject(UKDataContext.DRIVERS_CLASS_NAME.toString(), UKDataContext.SYSTEM_ORGI) == null) {
			CacheHelper.getSystemCacheBean().put(UKDataContext.DRIVERS_CLASS_NAME.toString(), drivers_class_name, UKDataContext.SYSTEM_ORGI);
		}
		
		//把预警用户放入缓存
		SystemConfig systemConfig = UKTools.getSystemConfig() ;
		if (!StringUtils.isBlank(systemConfig.getReqlogwarningtouser())) {
			UserRepository userRes = UKDataContext.getContext().getBean(UserRepository.class);
    		List<User> userList = new ArrayList<User>();
    		String[] userids = systemConfig.getReqlogwarningtouser().split(",");
    		if (userids != null && userids.length >0) {
    			for(String id : userids) {
    				User ut = userRes.findById(id) ;
    				userList.add(ut);
    			}
			}
    		CacheHelper.getSystemCacheBean().put(UKDataContext.SYSTEM_CACHE_WARNING_TOUSER, userList , UKDataContext.SYSTEM_ORGI);
		}
		
    }
}